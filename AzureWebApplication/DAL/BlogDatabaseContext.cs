﻿using System;
using System.Data.Entity;

namespace AzureWebApplication.Models
{
	public class BlogDatabaseContext : DbContext
	{
		public BlogDatabaseContext()
		{
			Database.SetInitializer(new BlogDatabaseInitializer());
		}

		public DbSet<Tag> Tags { get; set; }
		public DbSet<Post> Posts { get; set; }
		public DbSet<Option> Options { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Post>()
				.HasMany<Tag>(s => s.Tags)
				.WithMany(c => c.Posts)
				.Map(cs =>
				{
					cs.MapLeftKey("TagId");
					cs.MapRightKey("PostId");
					cs.ToTable("Post2Tag");
				});

			base.OnModelCreating(modelBuilder);
		}
	}

	public class BlogDatabaseInitializer : DropCreateDatabaseAlways<BlogDatabaseContext>
	{
		protected override void Seed(BlogDatabaseContext context)
		{
			context.Posts.Add(new Post
			{
				Title = "Хостинг ASP NET",
				Content = "lorem DELIMITER ipsum",
				Published = true,
				Tags = new[] {new Tag {Id = 1, Name = "ASP NET"}, new Tag {Id = 2, Name = "Hosting"}},
				CreateTime = DateTime.Now
			});

			context.Options.Add(new Option
			{
				Key = "blog-name",
				Name = "Название блога",
				Value = "Блог начинающего .NET разработчика"
			});

			context.SaveChanges();
		}
	}
}