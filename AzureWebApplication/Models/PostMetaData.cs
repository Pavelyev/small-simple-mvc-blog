using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AzureWebApplication.Models
{
	public class PostMetaData
	{
		[DisplayName("���������")]
		[Required]
		public string Title { get; set; }

		[DisplayName("�������")]
		[DataType(DataType.MultilineText)]
		[Required]
		public string Content { get; set; }

		[DisplayName("�����������")]
		public bool Published { get; set; }

		[DisplayName("����� ��������")]
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
		public DateTime CreateTime { get; set; }

		[DisplayName("����")]
		public virtual ICollection<Tag> Tags { get; set; }
	}
}