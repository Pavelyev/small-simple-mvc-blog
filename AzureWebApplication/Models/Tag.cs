using System.Collections.Generic;

namespace AzureWebApplication.Models
{
	public class Tag
	{
		public Tag()
		{
			Posts = new List<Post>();
		}
		public int Id { get; set; }
		public string Name { get; set; }
		public int Count { get; set; }
		public virtual ICollection<Post> Posts { get; set; }
	}
}