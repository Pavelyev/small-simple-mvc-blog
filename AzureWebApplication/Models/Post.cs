﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace AzureWebApplication.Models
{
	[MetadataType(typeof (PostMetaData))]
	public class Post
	{
		public Post()
		{
			Tags = new List<Tag>();
		}

		public int Id { get; set; }
		public string Title { get; set; }

		[AllowHtml]
		public string Content { get; set; }

		public bool Published { get; set; }
		public DateTime CreateTime { get; set; }
		//public DateTime UpdateTime { get; set; }
		public virtual ICollection<Tag> Tags { get; set; }

		[NotMapped]
		public string ShortDesc
		{
			get { return Content.Substring(0, Content.IndexOf("DELIMITER")); }
		}

		[NotMapped]
		public string Full
		{
			get { return Content.Replace("DELIMITER", ""); }
		}
	}
}