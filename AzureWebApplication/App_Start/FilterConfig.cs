﻿using System.Web;
using System.Web.Mvc;
using AzureWebApplication.Application;

namespace AzureWebApplication
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LogExceptionFilter());
        }
    }
}
