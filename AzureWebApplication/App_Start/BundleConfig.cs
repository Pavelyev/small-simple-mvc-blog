﻿using System.Web;
using System.Web.Optimization;

namespace AzureWebApplication
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
						//"~/Scripts/jquery-{version}.js",
						"~/Scripts/jquery.validate.js",
						//"~/Scripts/globalize/globalize.js",
						"~/Scripts/globalize/cultures/globalize.culture.ru-RU.js",
						"~/Scripts/jquery-ui-{version}.js",
						"~/Scripts/application.js"
						));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
						"~/Content/themes/flat/jquery-ui-{version}.css",
						"~/Content/site.css"
						));
        }
    }
}
