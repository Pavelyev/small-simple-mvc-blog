﻿using System.Web.Mvc;
using MarkdownLog;

namespace AzureWebApplication.Application
{
	public static class HtmlHelperExtensions
	{
		public static MvcHtmlString MarkDown(this HtmlHelper helper, string markdown)
		{
			return new MvcHtmlString(new MarkdownToHtmlConverter().Transform(markdown));
		}
	}
}