﻿using System.Runtime.Remoting;
using System.Web.Mvc;
using NLog;

namespace AzureWebApplication.Application
{
	public class LogExceptionFilter : IExceptionFilter
	{
		public void OnException(ExceptionContext filterContext)
		{
			var serverEx = filterContext.Exception;// as ServerException;
			if (serverEx == null)
				return;

			var logger = new LogFactory().GetCurrentClassLogger();
			logger.Fatal(serverEx.Message);
			logger.Fatal(serverEx.InnerException);
			logger.Fatal(serverEx.StackTrace);
		}
	}
}