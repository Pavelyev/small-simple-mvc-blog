﻿using System;
using System.Configuration;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace AzureWebApplication.Application
{
	public static class AuthComponent
	{
		public static void Auth(Object sender)
		{
			var app = sender as HttpApplication;
			if (app == null)
				return;

			var authCookie = app.Context.Request.Cookies.Get("AUTH");
			if (authCookie == null || String.IsNullOrEmpty(authCookie.Value))
				return;

			var ticket = FormsAuthentication.Decrypt(authCookie.Value);
			if (ticket != null && ticket.Name == GetPassword())
				app.Context.User = new Principal {Identity = new Identity {IsAuthenticated = true, Name = "Admin"}};
		}

		/// <summary>
		///     Returns blog password.
		///     Modify the method to suit your requirements.
		/// </summary>
		/// <returns></returns>
		private static string GetPassword()
		{
			return ConfigurationManager.AppSettings["blog-password"];
		}
	}


	public class Principal : IPrincipal
	{
		public bool IsInRole(string role)
		{
			throw new NotImplementedException();
		}

		public IIdentity Identity { get; set; }
	}

	public class Identity : IIdentity
	{
		public string Name { get; set; }
		public string AuthenticationType { get; set; }
		public bool IsAuthenticated { get; set; }
	}
}