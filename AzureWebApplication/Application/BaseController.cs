﻿using System.Web.Mvc;
using AzureWebApplication.Models;
using NLog;

namespace AzureWebApplication.Application
{
	public class BaseController : Controller
	{
		protected BlogDatabaseContext db { get; private set; }
		protected Logger logger { get; private set; }

		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			db = new BlogDatabaseContext();
			logger = LogManager.GetCurrentClassLogger();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && db != null)
				db.Dispose();
			base.Dispose(disposing);
		}
	}
}