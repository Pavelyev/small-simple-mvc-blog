﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AzureWebApplication.Application;
using AzureWebApplication.Models;

namespace AzureWebApplication.Areas.Admin.Controllers
{
	[Authorize]
	public class PostController : BaseController
	{
		// GET: Admin/Post
		public ActionResult Index()
		{
			return View(db.Posts.Include(x => x.Tags).OrderByDescending(x => x.CreateTime).Take(20));
		}

		public ActionResult Publish(int id)
		{
			db.Posts.Find(id).Published = true;
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		public ActionResult UnPublish(int id)
		{
			db.Posts.Find(id).Published = false;
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		// GET: Admin/Post/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: Admin/Post/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Title,Content")] Post post)
		{
			post.CreateTime = DateTime.Now;
			if (ModelState.IsValid)
			{
				db.Posts.Add(post);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			return View(post);
		}

		[HttpGet]
		public ActionResult Edit(int id)
		{
			var post = db.Posts.Find(id);
			if (post == null)
			{
				return HttpNotFound();
			}
			return View(post);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(int id, [Bind(Include = "Title,Content")] Post model)
		{
			var originalTag = db.Posts.Find(id);
			if (originalTag == null)
				return HttpNotFound();
			originalTag.Title = model.Title;
			originalTag.Content = model.Content;

			if (ModelState.IsValid)
			{
				db.Entry(originalTag).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			return View(model);
		}

		// GET: Admin/Post/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var post = db.Posts.Find(id);
			if (post == null)
			{
				return HttpNotFound();
			}
			return View(post);
		}

		// POST: Admin/Post/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			var post = db.Posts.Find(id);
			db.Posts.Remove(post);
			db.SaveChanges();
			return RedirectToAction("Index");
		}
	}
}