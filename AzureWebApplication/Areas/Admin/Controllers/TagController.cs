﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using AzureWebApplication.Application;
using AzureWebApplication.Areas.Admin.Models;
using AzureWebApplication.Models;

namespace AzureWebApplication.Areas.Admin.Controllers
{
	public class TagController : BaseController
	{
		public ActionResult Add(int postId)
		{
			var post = db.Posts.Find(postId);

			if (post == null)
				return HttpNotFound();

			ViewBag.tagId = new SelectList(db.Tags, "Id", "Name");

			return View();
		}

		[HttpPost]
		public ActionResult Add(int postId, AddTagViewModel model)
		{
			var post = db.Posts.Find(postId);

			if (post == null)
				return HttpNotFound();

			if (model.Tagid.HasValue)
			{
				var tag = db.Tags.Find(model.Tagid.Value);
				if (tag == null)
					return HttpNotFound();

				post.Tags.Add(tag);
				db.SaveChanges();
			}
			else
			{
				db.Tags.Add(new Tag
				{
					Name = model.TagName,
					Posts = new List<Post> {post}
				});
			}

			db.SaveChanges();

			return RedirectToAction("Index", "Post");
		}

		public ActionResult Recalc()
		{
			var tags = db.Tags.Include(x => x.Posts);
			foreach (var tag in tags)
			{
				tag.Count = tag.Posts.Count(x => x.Published);
			}
			db.SaveChanges();
			return RedirectToAction("Index", "Post");
		}

		public ActionResult Manage(int postId)
		{
			var post = db.Posts.Find(postId);
			if (post == null)
				return HttpNotFound();
			return View(post);
		}

		public ActionResult Delete(int postId, int tagId)
		{
			var tag = db.Tags.Find(tagId);
			var post = db.Posts.Find(postId);
			if (tag == null || post == null)
				return HttpNotFound();

			post.Tags.Remove(tag);
			db.SaveChanges();

			return RedirectToAction("Manage", postId);
		}
	}
}