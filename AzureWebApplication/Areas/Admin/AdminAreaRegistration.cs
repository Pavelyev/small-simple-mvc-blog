﻿using System.Web.Mvc;

namespace AzureWebApplication.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
			context.Routes.MapMvcAttributeRoutes();
			
//            context.MapRoute(
//                "",
//				"Admin/EditPost-{id}",
//				new { action = "Edit", controller = "Post"}
//            );
            context.MapRoute(
                "",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", controller = "Post", id = UrlParameter.Optional }
            );
        }
    }
}