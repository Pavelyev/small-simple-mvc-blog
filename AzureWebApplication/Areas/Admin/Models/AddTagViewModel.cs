﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureWebApplication.Areas.Admin.Models
{
	public class AddTagViewModel
	{
		[DisplayName("Существующий тэг")]
		public int? Tagid { get; set; }
		[DisplayName("Новый тэг")]
		public string TagName { get; set; }
	}
}
