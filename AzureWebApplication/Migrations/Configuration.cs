using System.Data.Entity.Migrations;
using AzureWebApplication.Models;

namespace AzureWebApplication.Migrations
{
	internal sealed class Configuration : DbMigrationsConfiguration<BlogDatabaseContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = true;
		}

		protected override void Seed(BlogDatabaseContext context)
		{
			context.Options.AddOrUpdate(new Option
			{
				Key = "blog-name",
				Name = "�������� �����",
				Value = "���� ����������� ���-������������"
			});
		}
	}
}