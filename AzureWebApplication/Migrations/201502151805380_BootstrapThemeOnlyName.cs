namespace AzureWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BootstrapThemeOnlyName : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.BootstrapThemes");
            AlterColumn("dbo.BootstrapThemes", "Name", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.BootstrapThemes", "Name");
            DropColumn("dbo.BootstrapThemes", "Id");
            DropColumn("dbo.BootstrapThemes", "Link");
            DropColumn("dbo.BootstrapThemes", "Image");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BootstrapThemes", "Image", c => c.String());
            AddColumn("dbo.BootstrapThemes", "Link", c => c.String());
            AddColumn("dbo.BootstrapThemes", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.BootstrapThemes");
            AlterColumn("dbo.BootstrapThemes", "Name", c => c.String());
            AddPrimaryKey("dbo.BootstrapThemes", "Id");
        }
    }
}
