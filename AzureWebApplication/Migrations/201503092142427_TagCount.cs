namespace AzureWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TagCount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tags", "Count", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tags", "Count");
        }
    }
}
