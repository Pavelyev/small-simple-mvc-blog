namespace AzureWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeletedThemesAddedOptions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Options",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        Value = c.String(),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Key);
            
            DropTable("dbo.BootstrapThemes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.BootstrapThemes",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                        Selected = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Name);
            
            DropTable("dbo.Options");
        }
    }
}
