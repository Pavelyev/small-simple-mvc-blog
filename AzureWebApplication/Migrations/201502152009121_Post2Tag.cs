namespace AzureWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Post2Tag : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Post2Tag",
                c => new
                    {
                        TagId = c.Int(nullable: false),
                        PostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TagId, t.PostId })
                .ForeignKey("dbo.Posts", t => t.TagId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.PostId, cascadeDelete: true)
                .Index(t => t.TagId)
                .Index(t => t.PostId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Post2Tag", "PostId", "dbo.Tags");
            DropForeignKey("dbo.Post2Tag", "TagId", "dbo.Posts");
            DropIndex("dbo.Post2Tag", new[] { "PostId" });
            DropIndex("dbo.Post2Tag", new[] { "TagId" });
            DropTable("dbo.Post2Tag");
        }
    }
}
