namespace AzureWebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BootstrapThemeImageAndLink : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BootstrapThemes", "Image", c => c.String());
            AddColumn("dbo.BootstrapThemes", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BootstrapThemes", "Name");
            DropColumn("dbo.BootstrapThemes", "Image");
        }
    }
}
