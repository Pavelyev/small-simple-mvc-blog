﻿using System.Web.Mvc;
using AzureWebApplication.Application;

namespace AzureWebApplication.Controllers
{
	public class OptionController : BaseController
	{
		[ChildActionOnly]
		public ActionResult Value(string key)
		{
			return new ContentResult {Content = db.Options.Find(key).Value};
		}
	}
}