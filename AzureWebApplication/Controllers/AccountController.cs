﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AzureWebApplication.Controllers
{
	public class AccountController : Controller
	{
		[HttpGet]
		[Route("Login")]
		public ActionResult Login()
		{
			return View();
		}

		[HttpPost]
		[Route("Login")]
		public ActionResult Login(string ReturnUrl, string password)
		{
			CreateCookie(password);
			if (Url.IsLocalUrl(ReturnUrl))
				return Redirect(ReturnUrl);
			return RedirectToAction("Index", "Home");
		}

		private void CreateCookie(string userName, bool isPersistent = false)
		{
			var ticket = new FormsAuthenticationTicket(
				1,
				userName,
				DateTime.Now,
				DateTime.Now.Add(FormsAuthentication.Timeout),
				isPersistent,
				string.Empty,
				Request.ApplicationPath
				);

			// Encrypt the ticket.
			var encTicket = FormsAuthentication.Encrypt(ticket);

			// Create the cookie.
			HttpContext.Response.Cookies.Set(new HttpCookie("AUTH")
			{
				Value = encTicket,
				Expires = DateTime.Now.Add(FormsAuthentication.Timeout),
				Path = Request.ApplicationPath
			});
		}

		[Authorize]
		[Route("Exit")]
		public ActionResult Logout()
		{
			if (Request.Cookies["AUTH"] != null)
			{
				var c = new HttpCookie("AUTH")
				{
					Expires = DateTime.Now.AddDays(-1),
					Path = Request.ApplicationPath
				};
				Response.Cookies.Add(c);
			}
			return Redirect(Url.RouteUrl("Login"));
		}
	}
}