﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using AzureWebApplication.Application;

namespace AzureWebApplication.Controllers
{
	public class HomeController : BaseController
	{
		public ActionResult Index()
		{
			var model = db.Posts.Where(x => x.Published).Include(x => x.Tags).OrderByDescending(x => x.CreateTime).Take(20);
			logger.Trace("taken " + model.Count() + " posts");
			return View(model);
		}

		[Route("About")]
		public ActionResult About()
		{
			return View();
		}

		[Route("Cv")]
		public ActionResult Cv()
		{
			return View();
		}

		[Route("tag-{tagId}")]
		public ActionResult Posts(int tagId)
		{
			var tag = db.Tags.Find(tagId);
			if (tag == null)
			{
				return HttpNotFound();
			}
			return View(tag);
		}

		[Route("post-{id}")]
		public ActionResult Post(int id)
		{
			var post = db.Posts.Where(x => x.Id == id).Include(x => x.Tags).First();
			if (post == null)
			{
				return HttpNotFound();
			}
			return View(post);
		}

		[ChildActionOnly]
		public ActionResult TagList()
		{
			return PartialView(db.Tags.Where(x => x.Count > 0).ToList());
		}
	}
}